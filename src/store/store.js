import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import CognitoAuth from 'vue-auth-cognito'
import cognitoConfig from '../config/cognito'

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [createPersistedState()],
  modules: {
    cognito: new CognitoAuth(cognitoConfig)
  }
})
