// import App from '../App.vue'
import DashboardLayout from '../components/dashboard.vue'
import Login from '../components/login.vue'
import Categories from '../components/categories.vue'
import Products from '../components/products.vue'

const routes = [
  {
    path: '/',
    component: DashboardLayout,
    children: [
      {path: '/products', component: Products},
      {path: '/categories', component: Categories}
    ]
  },
  {path: '/login', component: Login}
]

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
 function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes
